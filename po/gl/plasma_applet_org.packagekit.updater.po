# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marce Villarino <mvillarino@kde-espana.es>, 2013.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-10-29 01:54+0000\n"
"PO-Revision-Date: 2017-10-04 18:59+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/ui/ChangelogView.qml:31
msgid "Failed to get update details"
msgstr "Non se puideron obter detalles da actualización"

#: package/contents/ui/ChangelogView.qml:94
msgid "Version: %1"
msgstr "Versión: %1"

#: package/contents/ui/ChangelogView.qml:100
msgid "Updates: %1"
msgstr "Actualizacións: %1"

#: package/contents/ui/main.qml:39
msgid "Software Updates"
msgstr "Actualizacións de sofware"

#: package/contents/ui/main.qml:65 package/contents/ui/main.qml:218
msgid "Check for new updates"
msgstr "Comprobar se hai novas actualizacións"

#: package/contents/ui/main.qml:119
msgid "Failed to get updates"
msgstr "Non se puideron obter actualizacións"

#: package/contents/ui/main.qml:164
msgid "You have one update"
msgid_plural "You have %1 updates"
msgstr[0] "Hai unha actualización"
msgstr[1] "Hai %1 actualizacións"

#: package/contents/ui/main.qml:169
msgid "Your system is up to date"
msgstr "O sistema está posto ao día"

#: package/contents/ui/Transaction.qml:76
msgid "Cancel"
msgstr "Cancelar"

#: package/contents/ui/Updates.qml:79
msgid "Install"
msgstr "Instalar"